package ext;
/*
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.Collections;
*/
import java.util.List;
/*
import java.util.ArrayList;
import java.util.Objects;
import java.util.Iterator;
*/
import java.math.BigDecimal;

import jp.co.canon_soft.wp.runtime.bp.BpContext;
import jp.co.canon_soft.wp.runtime.bp.BusinessProcess;
/*
import wpapp.dm.DmCmdPRINT_DM;
import wpapp.dm.DmPRINT_DM;
import wpapp.dm.Dm_BeanFactory;
*/

public class EXT_WAIT implements BusinessProcess {  // インターフェースBusinessProcessを実装します。

	public List execute(BpContext context) {
		
		//　引数を取得する
		BigDecimal	decWaitMs	=	(BigDecimal)context.getArg("@1");	// 待機秒数(ms)
		
		try{
			Thread.sleep(decWaitMs.longValue());	//5000ミリ秒=5秒Sleepする
		} catch(Exception e){
		}
		return null;
	}
}
