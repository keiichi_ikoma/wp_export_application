package ext;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.net.URI;

import java.math.BigDecimal;

import jcifs.Config;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

import jp.co.canon_soft.wp.runtime.DispDataQuerier;
import jp.co.canon_soft.wp.runtime.dm.DmBroker;
import jp.co.canon_soft.wp.runtime.dm.DmBrokerFactory;
import jp.co.canon_soft.wp.runtime.dm.DmTransaction;
import jp.co.canon_soft.wp.runtime.file.DFile;
import jp.co.canon_soft.wp.runtime.file.DFileUtils;

import wpapp.dm.Dm_BeanFactory;
import wpapp.dm.DmT_FILESYSTEM;

public class FILE_API_DTL_DISP_JCIFS extends DispDataQuerier {

	// 物理ファイルの格納場所
	protected Collection queryDispData(DmTransaction tran, List params) throws Exception {
	
		//jcifs properties SV148
		Properties prop = new Properties ();
		prop.setProperty ("jcifs.netbios.wins", "10.254.2.8");
		prop.setProperty ("jcifs.smb.client.username", "ncadmin");
		prop.setProperty ("jcifs.smb.client.password", "vi8h89eUgN");
		Config.setProperties (prop);
	
		//params.get(n-1)
		String DEST_DIR = (String) params.get(1); //get(2)="\\sv148\CreateForm\PDF\***\out\"
		String SMB_DIR = ("smb://ncadmin:vi8h89eUgN@SV148/EXPORT_APPLICATION001_export_out/" + (String) params.get(3)); //get(4) = PDF File Name
		List results = new ArrayList();
		if (params.size() < 1) {
			return null;
		}
		PreparedStatement stmt = null;
		DmBroker broker = DmBrokerFactory.getBroker(tran);
		try {
			// JDBCコネクションを取得します
			Connection con = broker.getConnection();

			// 検索SQLを実行します。
			String sql = "SELECT REQUEST_NO,  FNAME, FSIZE, FTYPE"
					+ " FROM T_FILESYSTEM WHERE REQUEST_NO = ?";  //EDIT
			stmt = con.prepareStatement(sql);
			stmt.setString(1, (String) params.get(0));
			//stmt.setString(2, (String) params.get(1));
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {

				// 検索結果をbeanに格納します
				DmT_FILESYSTEM dm = Dm_BeanFactory.createT_FILESYSTEM();
				dm.setREQUEST_NO(rs.getBigDecimal("REQUEST_NO")); //EDIT
				//dm.setLINE_NO(rs.getBigDecimal("LINE_NO")); //EDIT
				String fname = rs.getString("FNAME");
				dm.setFNAME(fname);
				dm.setFTYPE(rs.getString("FTYPE"));
				//ファイルが存在した場合、物理ファイルをbeanに格納します
				if (fname != null && fname.length() > 0) {
					
					//JCIFS
					SmbFileInputStream SFIS = null;					
					SmbFile Sfile = new SmbFile(SMB_DIR);
					SFIS = new SmbFileInputStream(Sfile);
					
					//OS judge
					String OS_NAME = System.getProperty("os.name").toLowerCase();
		
					if(OS_NAME.startsWith("linux")){
											
						//Linux
						//mkdir /var/tmp/pdf
						File TEMP = new File("/var/tmp/pdf");
						if ( TEMP.exists()){
						} else {
							TEMP.mkdir();
						}
					
						//Remake dir /var/tmp/pdf/QuotationTEMP_@USER
						File PDF = new File("/var/tmp/pdf/" + "Export_TEMP_" + (String) params.get(4)); //EDIT
						if ( PDF.exists()){
							FileUtils.deleteDirectory(PDF);
						} 
						PDF.mkdir();
					
						//PDF remake /var/tmp/pdf/QuotationTEMP_@USER/*.pdf
						File tmp = new File("/var/tmp/pdf/" + "Export_TEMP_" + (String) params.get(4) + "/" +(String) params.get(3)); //EDIT
						if ( tmp.exists()) {
							tmp.delete();
						}
						tmp.createNewFile();
						
						InputStream is = Sfile.getInputStream();
						FileUtils.copyInputStreamToFile(is,tmp);										

						if(tmp.exists()){
							DFile dFile = DFileUtils.createFile(tmp);
							dFile.setContentSize(rs.getBigDecimal("FSIZE"));
							//dFile.setContentType(rs.getString("FTYPE"));
							dm.setFSIZE(rs.getBigDecimal("FSIZE"));
							//dm.setFTYPE(rs.getString("FTYPE"));
							dm.setFILE_DATA(dFile);
						}
						
						
					} else {
												
						//windows
						//mkdir c:\\TEMP
						File TEMP = new File("c:\\TEMP");
						if ( TEMP.exists()){
						} else {
							TEMP.mkdir();
						}
					
						//Remake dir c:\TEMP\@USER
						File PDF = new File("c:\\TEMP\\" + "Export_TEMP_" + (String) params.get(4)); //EDIT
						if ( PDF.exists()){
							FileUtils.deleteDirectory(PDF);
						}	 
						PDF.mkdir();
	
						//PDF remake c:\TEMP\@USER\QuotationTEMP_@USER\*.pdf
						File tmp = new File("c:\\TEMP\\" + "Export_TEMP_" + (String) params.get(4) + "\\" +(String) params.get(3)); //EDIT
						if ( tmp.exists()) {
							tmp.delete();
						}
						tmp.createNewFile();
						
						InputStream is = Sfile.getInputStream();
						FileUtils.copyInputStreamToFile(is,tmp);										

						if(tmp.exists()){
							DFile dFile = DFileUtils.createFile(tmp);
							dFile.setContentSize(rs.getBigDecimal("FSIZE"));
							//dFile.setContentType(rs.getString("FTYPE"));
							dm.setFSIZE(rs.getBigDecimal("FSIZE"));
							//dm.setFTYPE(rs.getString("FTYPE"));
							dm.setFILE_DATA(dFile);
						}

					}


					/*
					InputStream is = Sfile.getInputStream();
					FileUtils.copyInputStreamToFile(is,tmp);										

					if(tmp.exists()){
						DFile dFile = DFileUtils.createFile(tmp);
						dFile.setContentSize(rs.getBigDecimal("FSIZE"));
						//dFile.setContentType(rs.getString("FTYPE"));
						dm.setFSIZE(rs.getBigDecimal("FSIZE"));
						//dm.setFTYPE(rs.getString("FTYPE"));
						dm.setFILE_DATA(dFile);
					}
					*/
				}
				results.add(dm);
			}
			return results;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
					} catch (SQLException e1) {
				}
			}
		broker.close();
		}
	}
}