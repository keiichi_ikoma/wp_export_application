package ext;

import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Iterator;
import java.util.Properties;

import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.UnknownHostException;

import jcifs.Config;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

import jp.co.canon_soft.wp.runtime.bp.BpContext;
import jp.co.canon_soft.wp.runtime.bp.BusinessProcess;

import wpapp.dm.DmCmdPRINT_DM;
import wpapp.dm.DmPRINT_DM;
import wpapp.dm.Dm_BeanFactory;


public class EXT_PRINT_JCIFS implements BusinessProcess {  // インターフェースBusinessProcessを実装します。

	public List execute(BpContext context) {
	
	
		//jcifs properties SV148
	//	Properties prop = new Properties ();
	//	prop.setProperty ("jcifs.netbios.wins", "10.254.2.8");
	//	//prop.setProperty ("jcifs.smb.client.username", "ncadmin");
	//	//prop.setProperty ("jcifs.smb.client.password", "vi8h89eUgN");
	//	prop.setProperty ("jcifs.smb.client.username", "DTF");
	//	prop.setProperty ("jcifs.smb.client.password", "P53yQ8SnlU");


	//jcifs properties SV98
		Properties prop = new Properties ();
		prop.setProperty ("jcifs.netbios.wins", "192.168.2.7");
		//prop.setProperty ("jcifs.smb.client.username", "ncadmin");
		//prop.setProperty ("jcifs.smb.client.password", "vi8h89eUgN");
		prop.setProperty ("jcifs.smb.client.username", "DTF");
		prop.setProperty ("jcifs.smb.client.password", "P53yQ8SnlU");


		Config.setProperties (prop);

		//引数を取得する
		String	strDataModel	=	(String)context.getArg("@1");	// データを取得するデータモデル
		String	strPath			=	(String)context.getArg("@2");	// 出力先フォルダ csv
		String	strFile			=	(String)context.getArg("@3");	// 出力先ファイル csv
		String	strForm			=	(String)context.getArg("@4");	// 利用する帳票フォーム名
		String	strHead			=	(String)context.getArg("@5");	// ヘッダー情報の付加
																	// 		00:出力しない
																	//		01:項目名称を出力する
																	//		10:帳票フォーム名を出力する
																	//		20:アノテーションを出力する(Create!Form専用)
																	//		11:帳票フォーム名および項目名称を出力する
																	//		21:アノテーションおよび項目名称を出力する(Create!Form専用)
		String	strSep			=	(String)context.getArg("@6");	// 区切り文字の指定
																	//		0:無し
																	//		1:カンマ区切り
																	//		2:Tab区切り
		String	strEnc			=	(String)context.getArg("@7");	// 文字囲みの指定
																	//		0:無し
																	//		1:ダブルクォーテーション囲み
																	//		2:シングルクォーテーション囲み
		String	strOptS			=	(String)context.getArg("@8");	// ジョブファイル名(Create!Form専用)
		String	strOptP			=	(String)context.getArg("11");	// プリンタ名(Create!Form_PrintStage専用)
		BigDecimal	decOptNc	=	(BigDecimal)context.getArg("@12");	// 部数(Create!Form_PrintStage専用)
		String	strOptO_Path	=	(String)context.getArg("@9");	// 出力ファイルパス(Create!Form_Cast専用) pdf
		String	strOptO_File	=	(String)context.getArg("@10");	// 出力ファイル名(Create!Form_Cast専用) pdf
		String	strHikiValue13	=	(String)context.getArg("@13");	// 予備
		String	strHikiValue14	=	(String)context.getArg("@14");	// 予備
		String	strHikiValue15	=	(String)context.getArg("@15");	// 予備
		
		//String url = "smb://ncadmin:vi8h89eUgN@SV148/quotation_bak/0000006.csv";
		//CSV out  "QUOTATION001_quotation"
		//PDF "out"
		
		
		//csv url strPath = ShareName  StrFile = csvFileName
		//String csvurl = ("smb://ncadmin:vi8h89eUgN@SV148/" + strPath + "/" + strFile);
		String csvurl = ("smb://nissei-el.local;DTF:P53yQ8SnlU@SV148/" + strPath + "/" + strFile);
		//String csvurl = ("smb://nissei-el.local;DTF:P53yQ8SnlU@SV98/CP情報システム課/A.アプリ開発/201801_輸出依頼書/TEST/"+ strFile); //ファイルサーバーでテスト
		//String csvurl = ("smb://nissei-el.local;DTF:P53yQ8SnlU@SV148/CreateForm/PDF/EXPORT_APPLICATION001_export/"+ strFile); //パス直打ちのテスト
		
		//pdf url strOptO_Path = ShareName  strOptO_File = pdfFileName
		//String pdfurlfile = ("smb://ncadmin:vi8h89eUgN@SV148/" + strOptO_Path + "/" + strOptO_File);
		//String pdfurlpath = ("smb://ncadmin:vi8h89eUgN@SV148/" + strOptO_Path);
		
		String pdfurlfile = ("smb://nissei-el.local;DTF:P53yQ8SnlU@SV148/" + strOptO_Path + "/" + strOptO_File);
		String pdfurlpath = ("smb://nissei-el.local;DTF:P53yQ8SnlU@SV148/" + strOptO_Path);
		

		// データモデルのレコードを取得
	    List listFileSystem = context.getIn(strDataModel);
	    
	    // テキストデータの区切り文字の設定
    	String	strSeparator		=	"";
	    if (Objects.equals(strSep,"0"))	{
	    	// 区切り文字	:	無し(固定長)
	    	strSeparator		=	"";
	    } else if (Objects.equals(strSep,"1")) {
	    	// 区切り文字	:	,(カンマ)
	    	strSeparator		=	",";
	    } else if (Objects.equals(strSep,"2")) {
	    	// 区切り文字	:	Tab(タブ)
	    	strSeparator		=	"\t";
	    }

	    // テキストデータの文字囲み文字の設定
	    String	strEnclosure		=	"";
	    if (Objects.equals(strEnc,"0"))	{
	    	// 文字囲み文字	:	無し(固定長)
	    	strEnclosure		=	"";
	    } else if (Objects.equals(strEnc,"1")) {
	    	// 文字囲み文字	:	"(ダブルクォーテーション)
	    	strEnclosure		=	"\"";
	    } else if (Objects.equals(strEnc,"2")) {
	    	// 文字囲み文字	:	'(シングルクォーテーション)
	    	strEnclosure		=	"\'";
	    }
	    
	    // 出力データ領域
	    StringBuffer	strBuffer	=	new StringBuffer();
	    String			strData		=	"";
	    
	    // 処理ステータスの初期化
	    String 			ret			=	"0";
						
		try {
		
			SmbFile sf = new SmbFile(csvurl);
			
			// 出力対象ファイルの存在確認
			if ( sf.exists()) {
				// 既に存在する場合、出力対象ファイルを削除する
				sf.delete();
			}

			// 出力対象ファイルを空で作成する
			sf.createNewFile();
			//}catch( SmbException e){
			//	ret ="-9";
			//}
			
			SmbFileOutputStream os = null;
			os = new SmbFileOutputStream(sf);
			os.write(0xef);
			os.write(0xbb);
			os.write(0xbf);


			////　FileWriterクラスのオブジェクトを生成する　追記型
			//FileWriter file = new FileWriter(strPath + "\\" + strFile, true);
			//// PrintWriterクラスのオブジェクトを生成する
			//PrintWriter pw = new PrintWriter(new BufferedWriter(file));
			

			
			// PrintWriterクラスのオブジェクトを生成する
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os ,"UTF-8")));
			
			// 出力対象ファイルへ利用する帳票フォーム名またはアノテーションを追記する
			if ((Objects.equals(strHead,"10"))	||
				(Objects.equals(strHead,"11"))	)	{
				// 帳票フォーム名の出力
				// pw.println(strForm);
			} else if ((Objects.equals(strHead,"20"))	||
			           (Objects.equals(strHead,"21"))	)	{
				//  アノテーションの出力
				pw.println("<cf:mf-annotation>");						//開始タグ
				if ((strOptS != null)		&&
					(strOptS.isEmpty()	!=	true))	{
					pw.println("    cf.opt.s = " + strOptS);				//ジョブファイル名
				}
				if ((strOptP != null)		&&
					(strOptP.isEmpty()	!=	true))	{
					pw.println("    cf.opt.# = " + strOptP);				//プリンタ
				}
				if ((decOptNc != null)		&&
					(decOptNc.compareTo(BigDecimal.ZERO) != 0) )	{
					pw.println("    cf.opt.nc = " + decOptNc.toString());	//部数
				}
				if ((pdfurlpath != null)		&&
					(pdfurlpath.isEmpty()	!=	true))	{
					if ((pdfurlfile != null)		&&
						(pdfurlfile.isEmpty()	!=	true))	{
						pw.println("    cf.opt.o = " + pdfurlfile);				//出力ファイルパス・名　
					} else {
						pw.println("    cf.opt.o = " + pdfurlpath);				                    //出力ファイルパス
					}
				}
				pw.println("</cf:mf-annotation>");						//終了タグ
			}
			//}catch( SmbException e){
			//	ret =-7;
			//}
			
			// 出力対象ファイルへ項目名称を追記する
			if ((Objects.equals(strHead,"01"))	||
				(Objects.equals(strHead,"11"))	||
				(Objects.equals(strHead,"21"))  )	{

				// 退避領域のクリア
				strBuffer = new StringBuffer();
				
				// 1行目のデータを項目単位に分割する
				String strFileSystem	=	listFileSystem.get(0).toString();
				//String[] arrayData = strFileSystem.split(",");
				String[] arrayWork = strFileSystem.split(",");

				// ":"の存在の存在確認(":"が存在しない場合は、データ中に","があると判断して、前の項目に移動する"
				String[] arrayData = new String[arrayWork.length];
				int intArrayDataCount = -1;
				for (int intArrayWorkCount = 0; intArrayWorkCount < arrayWork.length; intArrayWorkCount++) {
					if (arrayWork[intArrayWorkCount].toString().indexOf(":") != -1) {
						// ":"が存在する場合
						intArrayDataCount++;
						arrayData[intArrayDataCount] = arrayWork[intArrayWorkCount].toString();
					} else {
						// ":"が存在しない場合
						arrayData[intArrayDataCount] = arrayData[intArrayDataCount].toString() + "," + arrayWork[intArrayWorkCount].toString(); 
					}
				}
				
				// 配列中のnullを削除する
				List<String> list = new ArrayList<String>(Arrays.asList(arrayData));
				list.removeAll(Collections.singleton(null)); 
				arrayData = list.toArray(new String[list.size()]);
				
				// 項目単位の繰り返し処理
				for (int intArrayCount = 0; intArrayCount < arrayData.length; intArrayCount++) {

                    // 項目idと項目内容に分割する
    				String[] arrayItemId = arrayData[intArrayCount].split(":", -1);

    				// 項目idを退避領域へ出力
    				strBuffer.append(strEnclosure + arrayItemId[0].toString() + strEnclosure);
    				if (intArrayCount < arrayData.length - 1) {
        				strBuffer.append(strSeparator);
        			}
					
				}
				
				// 退避領域の内容をテキストデータへ出力
				strData = strBuffer.toString();
				pw.println(strData);
				
			}
			
			// データ件数分の繰り替えし処理
			Iterator iteFileSystem = listFileSystem.iterator();
			while(iteFileSystem.hasNext()) {

				// 退避領域のクリア
				strBuffer = new StringBuffer();
				
				// 取得した1行データを項目単位に分割する
				String strFileSystem	=	iteFileSystem.next().toString();
				//String[] arrayData = strFileSystem.split(",");
				String[] arrayWork = strFileSystem.split(",");
				
				// ":"の存在の存在確認(":"が存在しない場合は、データ中に","があると判断して、前の項目に移動する"
				String[] arrayData = new String[arrayWork.length];
				int intArrayDataCount = -1;
				for (int intArrayWorkCount = 0; intArrayWorkCount < arrayWork.length; intArrayWorkCount++) {
					if (arrayWork[intArrayWorkCount].toString().indexOf(":") != -1) {
						// ":"が存在する場合
						intArrayDataCount++;
						arrayData[intArrayDataCount] = arrayWork[intArrayWorkCount].toString();
					} else {
						// ":"が存在しない場合
						arrayData[intArrayDataCount] = arrayData[intArrayDataCount].toString() + "," + arrayWork[intArrayWorkCount].toString(); 
					}
				}
				
				// 配列中のnullを削除する
				List<String> list = new ArrayList<String>(Arrays.asList(arrayData));
				list.removeAll(Collections.singleton(null)); 
				arrayData = list.toArray(new String[list.size()]);

				// 項目単位の繰り返し処理
				for (int intItemCount = 0; intItemCount < arrayData.length; intItemCount++) {

					// 項目idと項目内容に分割する
  					String[] arrayItem = arrayData[intItemCount].split(":", -1);

  					// 時刻等が含まれている場合、分割されるため、結合する
    				StringBuffer stritem = new StringBuffer();
    				for (int i = 1; i < arrayItem.length; i++) {
    					if (arrayItem[i] != null) {
    						if (i == 1) {
    							stritem.append(arrayItem[i]);
    						}else{
    							stritem.append( ":" + arrayItem[i]);
    						}
    					}
    				}
    					
    				// 項目内容を退避領域へ出力
    				strBuffer.append(strEnclosure + stritem.toString() + strEnclosure);
    				if (intItemCount < arrayData.length - 1) {
    					strBuffer.append(strSeparator);
    				}
					
				}
				
				// 退避領域の内容をテキストデータへ出力
				strData = strBuffer.toString();
				pw.println(strData);
				
			}
			
			// 出力対象ファイルを閉じる
			pw.close();

		} catch( MalformedURLException e){
			ret ="error:MalformedURLException" + e.getMessage();
		} catch( UnknownHostException e){
			ret ="error:UnknownHostException" + e.getMessage();
		} catch( SmbException e){
			ret ="error:SmbException " + e.getMessage();
		} catch( Exception e){
			ret ="error:Exception" + e.getMessage();
			
		}

		List retList = new ArrayList();
		DmPRINT_DM dm = Dm_BeanFactory.createPRINT_DM();
		//dm.setResult(BigDecimal.valueOf(ret));
		dm.setResult(String.valueOf(ret));
		retList.add(dm);
		return retList;
	}

    private static boolean MatchNullable(String s)
    {
         return (s == null) ? true : false;
    }

}
