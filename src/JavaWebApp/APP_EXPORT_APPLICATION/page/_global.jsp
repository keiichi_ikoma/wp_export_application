﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="jp.co.canon_soft.wp.runtime.AppContext"%>
<html>
<head>
<title>Custom header footer Sample</title>
</head>
<body>
<!-- TITLE { -->
  [<%=AppContext.getIoName()%> (<%=AppContext.getIoCode()%>)]
<!-- } TITLE -->

<!-- HEADER { -->
<link href="<%=AppContext.getContextPath()%>/style/customHF.css" rel="stylesheet" type="text/css">

<script language="JavaScript">
<!--
function confirmLogout(){
  myRet = confirm("Are you sure you want to log out ?");
  if ( myRet == true ) {
		location.href="<%=AppContext.getContextPath()%>/page/login.jsp";
	}
}
// -->
</script>

<div id="page_header">
<table><tr>
<td width="120px">
<span id="home"><a href="<%=AppContext.getContextPath()%>/_link.do?i=IO_10000"><img src="<%=AppContext.getContextPath()%>/image/nissei_logo.jpg" title="home"></a></span>
</td>
<td id="header_title">
<span id="title_text">【<%=AppContext.getAppName()%>】<%=AppContext.getIoName()%></span>
</td>

<td id="header_info" align="right" valign="bottom">
  <a href="#" onclick="changeLanguage('ja')">日本語</a>&nbsp;／&nbsp;<a href="#" onclick="changeLanguage('en')">英語</a>
  <span id="user_info" title="">Date : <%= AppContext.getToday() %>　Employee CD : <%= AppContext.getUser() %></span>
  <span id="logout"><a style="" onclick="confirmLogout()"><img src="<%=AppContext.getContextPath()%>/image/logout.png" title="Logout"></a></span>
</td>
</tr></table>
</div>
<!-- } HEADER -->

</body>
</html>