﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="jp.co.canon_soft.wp.runtime.AppContext"%>
<html>
<head>
<title>Custom header footer Sample</title>
</head>
<body>

<!-- HEADER { -->
<link href="<%=AppContext.getContextPath()%>/style/customHF.css" rel="stylesheet" type="text/css">

<div id="page_header">
<table><tr>
<td width="120px">
<span id="home"><a href="<%=AppContext.getContextPath()%>/_link.do?i=IO_10000"><img src="<%=AppContext.getContextPath()%>/image/nissei_logo.jpg" title="home"></a></span>
</td>
<td id="header_title"><span id="title_text">【<%=AppContext.getAppName()%>】Login</span></td>
</tr></table>
</div>

<!-- } HEADER -->

</body>
</html>
